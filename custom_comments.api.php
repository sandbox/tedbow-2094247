<?php
/**
 * @file
 * Hooks related custom_ctools_comments.
 *
 *
 */
/**
 * Implements hook_custom_ctools_comments_callbacks().
 *
 * Lets modules define callbacks that will provide selections of comments.
 * The callbacks should return an array of cid's.
 */
function hook_custom_ctools_comments_callbacks() {
  return array(
    'my_function' => array(
      'title' => t('My Title'),
    ),
  );
}