<?php
/**
 * Callback to get cid's for comments from authenticated users.
 */
function custom_ctools_comments_authenticated($node, $mode, $comments_per_page) {
  $query = _custom_ctools_comments_build_query($node, $mode, $comments_per_page);
  $query->condition('c.uid', 0, '!=');

  $cids = $query->execute()->fetchCol();
  return $cids;
}
/**
 * Callback to get cid's for comments from anonymous users.
 */
function custom_ctools_comments_custom_ctools_comments_anonymous($node, $mode, $comments_per_page) {
  $query = _custom_ctools_comments_build_query($node, $mode, $comments_per_page);
  $query->condition('c.uid', 0);

  $cids = $query->execute()->fetchCol();
  return $cids;
}