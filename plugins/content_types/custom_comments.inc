<?php
/**
 * @file
 * Ctools content type for custom comments type
 */
if (module_exists('comment')) {
  /**
   * Plugins are described by creating a $plugin array which will be used
   * by the system that includes this file.
   */
  $plugin = array(
    'single' => TRUE,
    'title' => t('Custom Node comments'),
    'icon' => 'icon_node.png',
    'description' => t('The custom selected comments of the referenced node.'),
    'required context' => new ctools_context_required(t('Node'), 'node'),
    'render callback' => 'custom_comments_content_type_render',
    'admin title' => 'custom_comments_content_type_admin_title',
    'category' => t('Node'),
    'defaults' => array(
      'mode' => variable_get('comment_default_mode', COMMENT_MODE_THREADED),
      'comments_per_page' => variable_get('comment_default_per_page', '50'),
      'comments_callback' => 'comment_get_thread',
    ),
    'edit form' => 'custom_comments_content_type_edit_form',
  );
}

function custom_comments_content_type_render($subtype, $conf, $panel_args, $context) {
  $node = isset($context->data) ? clone($context->data) : NULL;
  $block = new stdClass();
  $block->module = 'comments';
  $block->delta  = $node->nid;

  $block->title = t('Comments');
  if (empty($node)) {
    $block->content = t('Node comments go here.');
  }
  else if ($node->comment) {
    $block->content = custom_comments_comment_render($node, $conf);
    // Update the history table, stating that this user viewed this node.
    node_tag_new($node);
  }

  return $block;
}

function custom_comments_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['mode'] = array(
    '#type' => 'select',
    '#title' => t('Mode'),
    '#default_value' => $conf['mode'],
    '#options' => _comment_get_modes(),
    '#weight' => 1,
  );
  foreach (_comment_per_page() as $i) {
    $options[$i] = t('!a comments per page', array('!a' => $i));
  }
  $form['comments_per_page'] = array('#type' => 'select',
    '#title' => t('Pager'),
    '#default_value' => $conf['comments_per_page'],
    '#options' => $options,
    '#weight' => 3,
  );
  $form['comments_callback'] = array('#type' => 'select',
    '#title' => t('Comment Types'),
    '#default_value' => $conf['comments_callback'],
    '#options' => _custom_ctools_comments_callback_options(),
    '#weight' => 4,
  );
  return $form;
}

function custom_comments_content_type_edit_form_submit($form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

function custom_comments_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" custom comments', array('@s' => $context->identifier));
}

/**
 * This function is a somewhat stripped down version of comment_render
 * that removes a bunch of cruft that we both don't need, and makes it
 * difficult to modify this.
 */
function custom_comments_comment_render($node, $conf) {
  $output = '';
  if (!user_access('access comments') || !$node->comment) {
    return;
  }

  $mode = $conf['mode'];
  $comments_per_page = $conf['comments_per_page'];
  $callbacks = module_invoke_all('custom_ctools_comments_callbacks');
  $callback_info = $callbacks[$conf['comments_callback']];
  if (isset($callback_info['file'])) {
    $file = DRUPAL_ROOT . '/' . $callback_info['file'];
    if (is_file($file)) {
      require_once $file;
    }
  }
  $cids = call_user_func_array($conf['comments_callback'], array($node, $mode, $comments_per_page));
  $comments = comment_load_multiple($cids);

  if ($comments) {
    drupal_add_css(drupal_get_path('module', 'comment') . '/comment.css');
    comment_prepare_thread($comments);
    $build = comment_view_multiple($comments, $node);
    $build['pager']['#theme'] = 'pager';
    return drupal_render($build);
  }
  return;
}
function _custom_ctools_comments_callback_options() {
  $callbacks = module_invoke_all('custom_ctools_comments_callbacks');
  foreach ($callbacks as $callback => $callback_info) {
    if (is_array($callback_info)) {
      $options[$callback] = $callback_info['title'];
    }
    else {
      $options[$callback] = $callback_info;
    }
  }
  return $options;
}

